# i have sample basic js-rect project with dockerfile

# my structure
tree -I 'node_modules|dist|build'
.
└── my-react-app
    └── my-react-app
        ├── Dockerfile
        ├── README.md
        ├── k8s
        │   ├── deployment.yaml
        │   └── service.yaml
        ├── package-lock.json
        ├── package.json
        ├── public
        │   ├── favicon.ico
        │   ├── index.html
        │   ├── logo192.png
        │   ├── logo512.png
        │   ├── manifest.json
        │   └── robots.txt
        └── src
            ├── App.css
            ├── App.js
            ├── App.test.js
            ├── index.css
            ├── index.js
            ├── logo.svg
            ├── reportWebVitals.js
            └── setupTests.js
# deploy react
cd my-react-app/my-react-app
docker build -t my-react-app-image:latest .
docker tag my-react-app-image:latest yakovperets/my-react-matomo:latest
docker push yakovperets/my-react-matomo:latest
cd k8s
kubectl  apply -f deployment.yaml
kubectl port-forward svc/my-react-app-service 8080:80
# access on: http://localhost:8080/

# deploy matomo
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm install matomo bitnami/matomo
# result:
kubectl get svc
        NAME                   TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
        kubernetes             ClusterIP      10.96.0.1        <none>        443/TCP                      34d
        matomo                 LoadBalancer   10.107.69.204    localhost     80:31681/TCP,443:30943/TCP   14m
        matomo-mariadb         ClusterIP      10.101.94.223    <none>        3306/TCP                     14m
        my-react-app-service   NodePort       10.105.184.119   <none>        80:32638/TCP                 57m
kubectl get pods
        NAME                                       READY   STATUS      RESTARTS      AGE
        matomo-54b5fb988d-7t8xj                    1/1     Running     1 (13m ago)   15m
        matomo-archive-28642130-g5fzz              0/1     Completed   0             11m
        matomo-archive-28642135-dpbxm              0/1     Completed   0             6m2s
        matomo-archive-28642140-dtbrm              0/1     Completed   0             62s
        matomo-mariadb-0                           1/1     Running     0             15m
        matomo-scheduled-tasks-28642130-xs8tn      0/1     Completed   0             11m
        matomo-scheduled-tasks-28642135-kshnj      0/1     Completed   0             6m2s
        matomo-scheduled-tasks-28642140-254dm      0/1     Completed   0             62s
        my-react-app-deployment-664cc87c57-t89xr   1/1     Running     0             46m

kubectl port-forward svc/matomo 8081:80
# access-on: http://localhost:8081/
kubectl get secret --namespace default matomo -o yaml
echo "T3oxOHNId2ZhSw==" | base64 -d
# user
# Oz18sHwfaK

# the access to react and matomo was established. 
now i want to integrate my matomo with my react using:

cd my-react-app/my-react-app
npm install @datapunt/matomo-tracker-react

so untill now i using: matomo-wrapper
        const matomoInstance = createInstance({
        urlBase: 'http://localhost:8081', // Replace with your Matomo URL
        siteId: 1, // Replace with your Matomo site ID
        trackerUrl: 'http://localhost:8081/matomo.php', // Replace with your Matomo tracker URL
        srcUrl: 'http://localhost:8081/matomo.js', // Replace with your Matomo JS URL
        // Additional configuration options can be added here
        });

        ReactDOM.render(
        <React.StrictMode>
        <MatomoProvider value={matomoInstance}>
        <App />
        </MatomoProvider>
        </React.StrictMode>,
        document.getElementById('root')
        );
and inside my componnent i use: 
        useEffect(() => {
        // Track initial page view
        trackPageView({ title: 'Home Page' });

        // Cleanup function
        return () => {
        // Additional cleanup code if needed
        };
        }, []); // Run only once on component mount

        return (
        <div>
        <h1>My React App</h1>
        <button onClick={handleClick}>Track Event</button>
        {/* Your app content */}
        </div>
        );
        }
now i asking you: there is a way to use matomo without specify provider and each componnet tracker. just once in project conect to matomo and send all content? 
