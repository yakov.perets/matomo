// version2
import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();

// version1
// import React from 'react';
// import ReactDOM from 'react-dom';
// import { MatomoProvider, createInstance } from '@datapunt/matomo-tracker-react';
// import App from './App'; // Importing the App component

// const matomoInstance = createInstance({
//   urlBase: 'http://localhost:8081', // Replace with your Matomo URL
//   siteId: 1, // Replace with your Matomo site ID
//   trackerUrl: 'http://localhost:8081/matomo.php', // Replace with your Matomo tracker URL
//   srcUrl: 'http://localhost:8081/matomo.js', // Replace with your Matomo JS URL
//   // Additional configuration options can be added here
// });

// ReactDOM.render(
//   <React.StrictMode>
//     <MatomoProvider value={matomoInstance}>
//       <App />
//     </MatomoProvider>
//   </React.StrictMode>,
//   document.getElementById('root')
// );
