// vwersion2
import React from 'react'; // Add this line
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;

// version1
// import React, { useEffect } from 'react';
// import { useMatomo } from '@datapunt/matomo-tracker-react';

// function App() {
//   const { trackPageView, trackEvent } = useMatomo();

//   // Define handleClick function here
//   const handleClick = () => {
//     trackEvent({ category: 'Button', action: 'Click', name: 'Example Button' });
//   };

//   useEffect(() => {
//     // Track initial page view
//     trackPageView({ title: 'Home Page' });

//     // Cleanup function
//     return () => {
//       // Additional cleanup code if needed
//     };
//   }, []); // Run only once on component mount

//   return (
//     <div>
//       <h1>My React App</h1>
//       <button onClick={handleClick}>Track Event</button>
//       {/* Your app content */}
//     </div>
//   );
// }

// export default App;


